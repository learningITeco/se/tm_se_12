package ru.potapov.tm.endpoint;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.xml.ser.ToXmlGenerator;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.api.IRepository;
import ru.potapov.tm.api.ServiceLocator;
import ru.potapov.tm.dto.Data;
import ru.potapov.tm.dto.DataXml;
import ru.potapov.tm.dto.MyMap;
import ru.potapov.tm.entity.Project;
import ru.potapov.tm.entity.Session;
import ru.potapov.tm.entity.Task;
import ru.potapov.tm.entity.User;
import ru.potapov.tm.util.ValidateExeption;

import javax.jws.WebService;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.util.*;

@WebService(endpointInterface = "ru.potapov.tm.endpoint.IProjectEndpoint")
public final class ProjectEndpoint extends AbstractServiceEndpoint<Project> implements IProjectEndpoint {
    public ProjectEndpoint(@NotNull ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public @NotNull MyMap getProjectMapRepository() {
        return getServiceLocator().getProjectService().getProjectMapRepository();
    }

    @Override
    public int checkProjectSize(@NotNull Session session) throws ValidateExeption {
        getServiceLocator().getSessionService().validSession(session);        
        return getServiceLocator().getProjectService().checkProjectSize();
    }

    @Override
    public @Nullable Project findOneProject(@NotNull Session session, @NotNull String name) throws ValidateExeption{
        getServiceLocator().getSessionService().validSession(session);
        return getServiceLocator().getProjectService().findOneProject(name);
    }

    @Override
    public @Nullable Project findOneProjectById(@NotNull Session session, @NotNull String id) throws ValidateExeption{
        getServiceLocator().getSessionService().validSession(session);
        return getServiceLocator().getProjectService().findOneProjectById(id);
    }

    @Override
    public @Nullable Project findOneProjectByIdAndUserId(@NotNull Session session, @NotNull String userId, @NotNull String id) throws ValidateExeption{
        getServiceLocator().getSessionService().validSession(session);
        return getServiceLocator().getProjectService().findOneProjectByIdAndUserId(userId, id);
    }

    @Nullable
    @Override
    public Project findProjectByName(@NotNull Session session, @NotNull final String name) throws ValidateExeption{
        getServiceLocator().getSessionService().validSession(session);
        return getServiceLocator().getProjectService().findProjectByName(name);
    }

    @Nullable
    @Override
    public Project findProjectByNameAndUserId(@NotNull Session session, @NotNull final String userId, @NotNull final String name) throws ValidateExeption{
        getServiceLocator().getSessionService().validSession(session);
        return getServiceLocator().getProjectService().findProjectByNameAndUserId(userId, name);
    }

    @NotNull
    @Override
    public Collection<Project> getProjectCollection(@NotNull Session session, @NotNull final String userId) throws ValidateExeption{
        getServiceLocator().getSessionService().validSession(session);
        return getServiceLocator().getProjectService().getProjectCollection(userId);
    }

    @NotNull
    @Override
    public Project renameProject(@NotNull Session session, @NotNull final Project project, @Nullable final String name) throws CloneNotSupportedException, ValidateExeption{
        return getServiceLocator().getProjectService().renameProject(project, name);
    }

    @Override
    public void removeAllProjectByUserId(@NotNull Session session, @NotNull final String userId) throws ValidateExeption{
        getServiceLocator().getSessionService().validSession(session);
        getServiceLocator().getProjectService().removeAllProjectByUserId(userId);
    }
    
    @Override
    public void removeAllProject(@NotNull Session session, @NotNull final Collection<Project> listProjects) throws ValidateExeption{
        getServiceLocator().getSessionService().validSession(session);
        getServiceLocator().getProjectService().removeAllProject(listProjects);
    }
    
    @Override
    public void removeProject(@NotNull Session session, @NotNull final Project project) throws ValidateExeption{
        getServiceLocator().getSessionService().validSession(session);
        getServiceLocator().getProjectService().removeProject(project);
    }
    
    @Override
    public void putProject(@NotNull Session session, @NotNull final Project project) throws ValidateExeption{
        getServiceLocator().getSessionService().validSession(session);
        getServiceLocator().getProjectService().putProject(project);
    }

    @Override
    public void setProjectMapRepository(@NotNull final MyMap mapRepository){
        getServiceLocator().getProjectService().setProjectMapRepository(mapRepository);
    };

    @NotNull
    @Override
    public String collectProjectInfo(@NotNull Session session, @NotNull final Project project, @NotNull final String owener) throws ValidateExeption{
        getServiceLocator().getSessionService().validSession(session);
        return getServiceLocator().getProjectService().collectProjectInfo(project,owener);
    }

    //Save-Load
    @Override
    public void saveBinar(@Nullable Session session) throws Exception{
        getServiceLocator().getSessionService().validSession(session);
        getServiceLocator().getProjectService().saveBinar();
    }

    @Override
    public void loadBinar(@Nullable Session session) throws Exception{
        getServiceLocator().getSessionService().validSession(session);
        getServiceLocator().getProjectService().loadBinar();
    }
    @Override
    public void saveJaxb(@Nullable Session session, boolean formatXml) throws Exception {
        getServiceLocator().getSessionService().validSession(session);
        getServiceLocator().getProjectService().saveJaxb(formatXml);
    }
    @Override
    public void loadJaxb(@Nullable Session session, boolean formatXml) throws Exception {
        getServiceLocator().getSessionService().validSession(session);
        getServiceLocator().getProjectService().loadJaxb(formatXml);
    }

    @Override
    public void saveFasterXml(@Nullable Session session) throws Exception {
        getServiceLocator().getSessionService().validSession(session);
        getServiceLocator().getProjectService().saveFasterXml();
    }
    @Override
    public void loadFasterXml(@Nullable Session session) throws Exception {
        getServiceLocator().getSessionService().validSession(session);
        getServiceLocator().getProjectService().loadFasterXml();
    }

    @Override
    public void saveFasterJson(@Nullable Session session) throws Exception {
        getServiceLocator().getSessionService().validSession(session);
        getServiceLocator().getProjectService().saveFasterJson();
    }
    @Override
    public void loadFasterJson(@Nullable Session session) throws Exception {
        getServiceLocator().getSessionService().validSession(session);
        getServiceLocator().getProjectService().loadFasterJson();
    }
}

package ru.potapov.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.api.IRepository;
import ru.potapov.tm.api.ServiceLocator;
import ru.potapov.tm.dto.MyMap;
import ru.potapov.tm.entity.RoleType;
import ru.potapov.tm.entity.Session;
import ru.potapov.tm.entity.User;
import ru.potapov.tm.util.SignatureUtil;
import ru.potapov.tm.util.ValidateExeption;

import javax.jws.WebService;
import java.io.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@WebService(endpointInterface = "ru.potapov.tm.endpoint.IUserEndpoint")
public final class UserEndpoint extends AbstractServiceEndpoint<User> implements IUserEndpoint {
    @Nullable private User authorizedUser  = null;
    private boolean isAuthorized           = false;

    public UserEndpoint(@NotNull ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public @Nullable RoleType getUserRole(@NotNull Session session, @NotNull User user) throws ValidateExeption {
        getServiceLocator().getSessionService().validSession(session);
        return getServiceLocator().getUserService().getUserRole(user);
    }

    @Override
    public boolean isAdministrator(@NotNull Session session, @NotNull User user) throws ValidateExeption {
        getServiceLocator().getSessionService().validSession(session);
        return getServiceLocator().getUserService().isAdministrator(user);
    }

    @Override
    public void saveAllUsers() throws Exception {
        getServiceLocator().getUserService().saveAllUsers();
    }

    @Override
    public @Nullable User getUserByName(@NotNull String name) {
        return getServiceLocator().getUserService().getUserByName(name);
    }

    @Override
    public @NotNull Collection<User> getUserCollection() {
        return getServiceLocator().getUserService().getUserCollection();
    }

    @Override
    public @NotNull User createUser(@NotNull String name, @NotNull String hashPass, @NotNull RoleType role) {
        return getServiceLocator().getUserService().createUser(name, hashPass, role);
    }

    @Override
    public @Nullable User getUserByNamePass(@NotNull final String name, @NotNull final String pass) throws Exception {
        return getServiceLocator().getUserService().getUserByNamePass(name, pass);
    }

    @Nullable
    @Override
    public User getUserById(@Nullable Session session, @Nullable final String id) throws ValidateExeption{
        getServiceLocator().getSessionService().validSession(session);
        return getServiceLocator().getUserService().getUserById(id);
    }

    @NotNull
    @Override
    public User changePass(@Nullable Session session, @Nullable final User user, @Nullable final String newHashPass) throws CloneNotSupportedException, ValidateExeption{
        getServiceLocator().getSessionService().validSession(session);
        return getServiceLocator().getUserService().changePass(user,newHashPass);
    }

    @Override
    public void putUser(@Nullable Session session, @Nullable final User user) throws ValidateExeption{
        getServiceLocator().getSessionService().validSession(session);
        getServiceLocator().getUserService().putUser(user);
    }

    @NotNull
    @Override
    public String collectUserInfo(@Nullable Session session,@Nullable final User user) throws ValidateExeption{
        getServiceLocator().getSessionService().validSession(session);
        return getServiceLocator().getUserService().collectUserInfo(user);
    }
}

package ru.potapov.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.api.IRepository;
import ru.potapov.tm.api.ISessionService;
import ru.potapov.tm.api.ServiceLocator;
import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.endpoint.AbstractServiceEndpoint;
import ru.potapov.tm.endpoint.ISessionEndpoint;
import ru.potapov.tm.entity.Session;
import ru.potapov.tm.entity.User;
import ru.potapov.tm.util.JavaToMysql;
import ru.potapov.tm.util.SignatureUtil;
import ru.potapov.tm.util.ValidateExeption;

import javax.jws.WebService;
import java.util.*;

@Getter
@Setter
@NoArgsConstructor
@WebService(endpointInterface = "ru.potapov.tm.endpoint.ISessionEndpoint")
public final class SessionService extends AbstractService<Session> implements ISessionService {
    @NotNull final Map<String, Session> mapSession = new HashMap<>();
    @Nullable private Bootstrap bootstrap;


    public SessionService(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public SessionService(@NotNull ServiceLocator serviceLocator, @NotNull IRepository<Session> repository) {
        super(serviceLocator, repository);
    }

    @Override
    public boolean validSession(@NotNull Session session) throws ValidateExeption {
        @Nullable Session sessionClone;
        try { sessionClone = session.clone(); }catch (Exception e){return false;}
        sessionClone.setSignature("");
        @Nullable final String sign = SignatureUtil.sign(sessionClone,"",1);
        if ( sign.equals(session.getSignature()) ){
            final long timDif = (new Date().getTime() - session.getDateStamp().getTime());
            if ( timDif < 86400000 )
                return true;
        }
        return false;
    }

    @Override
    public @Nullable Session generateSession(@NotNull Session session, @NotNull User user) {
        @Nullable Session sessionClone;
        try { sessionClone = session.clone(); }catch (Exception e){return null;}
        sessionClone.setSignature("");
        session.setSignature(SignatureUtil.sign(sessionClone,"", 1));
        session.setUserId(user.getId());
        return session;
    }

    @Override
    public void addSession(Session session, String userId) throws ValidateExeption {
        getServiceLocator().getSessionService().validSession(session);
        try {
            JavaToMysql.begin();
            getRepository().persist(userId, session);
            JavaToMysql.getConnection().commit();
        }catch (Exception e){e.printStackTrace(); try { JavaToMysql.getConnection().rollback(); } catch (Exception e1){}}
        finally {JavaToMysql.close();}
    }

    @Override
    public @Nullable Collection<Session> getSessionCollection() {
        @Nullable Collection<Session> list = new ArrayList<>();
        try {
            JavaToMysql.begin();
            list = getRepository().getMapRepository().values();
            JavaToMysql.getConnection().commit();
        }catch (Exception e){e.printStackTrace(); try { JavaToMysql.getConnection().rollback(); } catch (Exception e1){}}
        finally {JavaToMysql.close();}
        return list;
    }

    @Override
    public void removeSession(@NotNull Session session) {
        try {
            JavaToMysql.begin();
            getRepository().remove(session);
            JavaToMysql.getConnection().commit();
        }catch (Exception e){e.printStackTrace(); try { JavaToMysql.getConnection().rollback(); } catch (Exception e1){}}
        finally {JavaToMysql.close();}
    }
}

package ru.potapov.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.api.ITerminalService;
import ru.potapov.tm.bootstrap.Bootstrap;

import java.util.*;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public final class TerminalService implements ITerminalService {
    @Nullable private Bootstrap bootstrap;
    @NotNull final private Scanner             in            = new Scanner(System.in);


    public TerminalService(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }


    @Override
    public void printMassageNotAuthorized(){
        printlnArbitraryMassage("You are not authorized, plz login (type command <user-login>)");
    }

    @Override
    public void printMassageCompleted(){
        printlnArbitraryMassage("Completed");
    }

    @Override
    public void printMassageOk(){
        printlnArbitraryMassage("Ok");
    }

    @Override
    public void printlnArbitraryMassage(String msg){
        printArbitraryMassage(msg + "\n");
    }

    @Override
    public void printArbitraryMassage(String msg){
        System.out.print(msg);
    }
}

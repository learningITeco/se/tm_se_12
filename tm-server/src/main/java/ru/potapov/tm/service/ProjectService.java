package ru.potapov.tm.service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.xml.ser.ToXmlGenerator;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.api.IProjectService;
import ru.potapov.tm.api.IRepository;
import ru.potapov.tm.api.ServiceLocator;
import ru.potapov.tm.dto.Data;
import ru.potapov.tm.dto.DataXml;
import ru.potapov.tm.dto.MyMap;
import ru.potapov.tm.endpoint.AbstractServiceEndpoint;
import ru.potapov.tm.endpoint.IProjectEndpoint;
import ru.potapov.tm.entity.Project;
import ru.potapov.tm.entity.Session;
import ru.potapov.tm.entity.Task;
import ru.potapov.tm.entity.User;
import ru.potapov.tm.util.JavaToMysql;
import ru.potapov.tm.util.ValidateExeption;

import javax.jws.WebService;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.sql.PreparedStatement;
import java.util.*;

@WebService(endpointInterface = "ru.potapov.tm.endpoint.IProjectEndpoint")
public final class ProjectService extends AbstractService<Project> implements IProjectService {
    public ProjectService(@NotNull ServiceLocator serviceLocator, @NotNull IRepository<Project> repository) {
        super(serviceLocator, repository);
    }

    @Override
    public @NotNull MyMap getProjectMapRepository() {
        return new MyMap(getRepository().getMapRepository());
    }

    @Override
    public int checkProjectSize() throws ValidateExeption {
        if (Objects.isNull(getRepository()) )
            return 0;
        int i = 0;
        try {
            JavaToMysql.begin();
            i = getRepository().getCollection().size();
            JavaToMysql.getConnection().commit();
        }catch (Exception e){e.printStackTrace(); try { JavaToMysql.getConnection().rollback(); } catch (Exception e1){}}
        finally {JavaToMysql.close();}
        return i;
    }

    @Override
    public @Nullable Project findOneProject(@NotNull String name) throws ValidateExeption{
        if (Objects.isNull(getRepository()))
            return null;
        @Nullable Project project = null;
        try {
            JavaToMysql.begin();
            project = getRepository().findOne(name);
            JavaToMysql.getConnection().commit();
        }catch (Exception e){e.printStackTrace(); try { JavaToMysql.getConnection().rollback(); } catch (Exception e1){}}
        finally {JavaToMysql.close();}

        return project;
    }

    @Override
    public @Nullable Project findOneProjectById(@NotNull String id) throws ValidateExeption{
        if (Objects.isNull(getRepository()))
            return null;

        @Nullable Project project = null;
        try {
            JavaToMysql.begin();
            project = getRepository().findOneById(id);
            JavaToMysql.getConnection().commit();
        }catch (Exception e){e.printStackTrace(); try { JavaToMysql.getConnection().rollback(); } catch (Exception e1){}}
        finally {JavaToMysql.close();}
        return project;
    }

    @Override
    public @Nullable Project findOneProjectByIdAndUserId(@NotNull String userId, @NotNull String id) throws ValidateExeption{
        if (Objects.isNull(getRepository()))
            return null;

        @Nullable Project project = null;
        try {
            JavaToMysql.begin();
            project = getRepository().findOneById(userId, id);
            JavaToMysql.getConnection().commit();
        }catch (Exception e){e.printStackTrace(); try { JavaToMysql.getConnection().rollback(); } catch (Exception e1){}}
        finally {JavaToMysql.close();}
        return project;
    }

    @Nullable
    @Override
    public Project findProjectByName(@NotNull final String name) throws ValidateExeption{
        if (Objects.isNull(getRepository()))
            return null;

        @Nullable Project project = null;
        try {
            JavaToMysql.begin();
            project = getRepository().findOne(name);
            JavaToMysql.getConnection().commit();
        }catch (Exception e){e.printStackTrace(); try { JavaToMysql.getConnection().rollback(); } catch (Exception e1){}}
        finally {JavaToMysql.close();}
        return project;
    }

    @Nullable
    @Override
    public Project findProjectByNameAndUserId(@NotNull final String userId, @NotNull final String name) throws ValidateExeption{
        if (Objects.isNull(getRepository()))
            return null;

        @Nullable Project project = null;
        try {
            JavaToMysql.begin();
            project = getRepository().findOne(userId, name);
            JavaToMysql.getConnection().commit();
        }catch (Exception e){e.printStackTrace(); try { JavaToMysql.getConnection().rollback(); } catch (Exception e1){}}
        finally {JavaToMysql.close();}
        return project;
    }

    @NotNull
    @Override
    public Collection<Project> getProjectCollection(@NotNull final String userId) throws ValidateExeption{
        if (Objects.isNull(getRepository()))
            return new ArrayList<>();

        @Nullable Collection<Project> list = new ArrayList<>();
        try {
            JavaToMysql.begin();
            @Nullable  final User user = getServiceLocator().getUserService().getUserById( userId);
            if ( getServiceLocator().getUserService().isAdministrator( user) )
                list = getRepository().getCollection();
            else
                list =  getRepository().getCollection(userId);
            JavaToMysql.getConnection().commit();
        }catch (Exception e){e.printStackTrace(); try { JavaToMysql.getConnection().rollback(); } catch (Exception e1){}}
        finally {JavaToMysql.close();}
        return list;
    }

    @NotNull
    @Override
    public Project renameProject(@NotNull final Project project, @Nullable final String name) throws CloneNotSupportedException, ValidateExeption{
        if (Objects.nonNull(name) && Objects.nonNull(getRepository()) ){
            @Nullable Project newProject = (Project) project.clone();
            newProject.setName(name);
            try {
                JavaToMysql.begin();
                getRepository().merge(newProject);
                JavaToMysql.getConnection().commit();
            }catch (Exception e){e.printStackTrace(); try { JavaToMysql.getConnection().rollback(); } catch (Exception e1){}}
            finally {JavaToMysql.close();}
        }
        return project;
    }

    @Override
    public void removeAllProjectByUserId(@NotNull final String userId) throws ValidateExeption{
        if (Objects.isNull(getRepository()))
            return;
        try {
            JavaToMysql.begin();
            getRepository().removeAll(userId);
            JavaToMysql.getConnection().commit();
        }catch (Exception e){e.printStackTrace(); try { JavaToMysql.getConnection().rollback(); } catch (Exception e1){}}
        finally {JavaToMysql.close();}
    }
    
    @Override
    public void removeAllProject(@NotNull final Collection<Project> listProjects) throws ValidateExeption{
        if (Objects.isNull(getRepository()))
            return;
        try {
            JavaToMysql.begin();
            getRepository().removeAll(listProjects);
            JavaToMysql.getConnection().commit();
        }catch (Exception e){e.printStackTrace(); try { JavaToMysql.getConnection().rollback(); } catch (Exception e1){}}
        finally {JavaToMysql.close();}
    }
    
    @Override
    public void removeProject(@NotNull final Project project) throws ValidateExeption{
        if (Objects.isNull(getRepository()))
            return;
        try {
            JavaToMysql.begin();
            getRepository().remove(project);
            JavaToMysql.getConnection().commit();
        }catch (Exception e){e.printStackTrace(); try { JavaToMysql.getConnection().rollback(); } catch (Exception e1){}}
        finally {JavaToMysql.close();}
    }
    
    @Override
    public void putProject(@NotNull final Project project) throws ValidateExeption{
        if (Objects.isNull(getRepository()))
            return;

        try {
            JavaToMysql.begin();
            if (Objects.isNull(findOneProjectById( project.getId())))
                getRepository().persist(project);
            else
                getRepository().merge(project);
            JavaToMysql.getConnection().commit();
        }catch (Exception e){e.printStackTrace(); try { JavaToMysql.getConnection().rollback(); } catch (Exception e1){}}
        finally {JavaToMysql.close();}
    }

    @Override
    public void setProjectMapRepository(@NotNull final MyMap mapRepository){
        try {
            JavaToMysql.begin();
            getRepository().setMapRepository(( Map<String, Project> )mapRepository.getMap());
            JavaToMysql.getConnection().commit();
        }catch (Exception e){e.printStackTrace(); try { JavaToMysql.getConnection().rollback(); } catch (Exception e1){}}
        finally {JavaToMysql.close();}
    };

    @NotNull
    @Override
    public String collectProjectInfo(@NotNull final Project project, @NotNull final String owener) throws ValidateExeption{
        @NotNull String res = "";

        if (Objects.isNull(project.getDateStart()) || Objects.isNull(project.getDateFinish()))
            return res;

        res += "\n";
        res += "Project [" + project.getName() + "]" +  "\n";
        res += "Owner: " + owener + "\n";
        res += "Status: " + project.getStatus() + "\n";
        res += "Description: " + project.getDescription() + "\n";
        res += "ID: " + project.getId() + "\n";
        res += "Date start: " + getServiceLocator().getFt().format(project.getDateStart() ) + "\n";
        res += "Date finish: " +  getServiceLocator().getFt().format(project.getDateFinish()) + "\n";

        return res;
    }

    //Save-Load
    @Override
    public void saveBinar() throws Exception{
        @NotNull final File file = new File("data-"+ getClassName() +".binar");
        @NotNull final ObjectOutputStream inputStream = new ObjectOutputStream(new FileOutputStream(file));

        @NotNull final Data data = new Data();
        data.setProjectMap( (Map<String, Project>)getServiceLocator().getProjectService().getProjectMapRepository().getMap() );
        data.setTaskMap( (Map<String, Task>) getServiceLocator().getTaskService().getTaskMapRepository().getMap() );

        inputStream.writeObject( data );
        inputStream.close();
    }

    @Override
    public void loadBinar() throws Exception{
        @NotNull final File file = new File("data-"+ getClassName() +".binar");
        if (!file.canRead()){
            getServiceLocator().getTerminalService().printlnArbitraryMassage("File cannot be opened");
            return;
        }

        @NotNull final ObjectInputStream inputStream = new ObjectInputStream( new FileInputStream(file) );
        @NotNull final Object dataObj = inputStream.readObject();
        if (dataObj instanceof Data) {
            @NotNull final Data data = (Data) dataObj;
            try {
                JavaToMysql.begin();
                getServiceLocator().getProjectService().setProjectMapRepository( new MyMap(data.getProjectMap()) );
                getServiceLocator().getTaskService().setTaskMapRepository( new MyMap(data.getTaskMap()) );
                JavaToMysql.getConnection().commit();
            }catch (Exception e){e.printStackTrace(); try { JavaToMysql.getConnection().rollback(); } catch (Exception e1){}}
            finally {JavaToMysql.close();}
        }
    }
    @Override
    public void saveJaxb(boolean formatXml) throws Exception {
        @NotNull final Data data = new Data() ;
        data.setProjectMap( (Map<String, Project>) getServiceLocator().getProjectService().getProjectMapRepository().getMap() );
        data.setTaskMap( (Map<String, Task>)getServiceLocator().getTaskService().getTaskMapRepository().getMap() );

        @NotNull final JAXBContext context      = JAXBContext.newInstance(Data.class) ;
        @NotNull final Marshaller marshaller    = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        @NotNull String typeFormatName = "xml";
        if (!formatXml){
            typeFormatName = "json";
            marshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
            marshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);
        }
        @NotNull final File file = new File("data-jaxb-"+getClassName()+"."+typeFormatName);
        marshaller.marshal(data, new FileWriter(file));
    }
    @Override
    public void loadJaxb(boolean formatXml) throws Exception {
        @NotNull final JAXBContext context = JAXBContext.newInstance(Data.class);
        @NotNull final Unmarshaller unmarshaller = context.createUnmarshaller();

        @NotNull String typeFormatName = "xml";
        if (!formatXml) {
            typeFormatName = "json";
            unmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
            unmarshaller.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, true);
        }

        @NotNull final File file = new File("data-jaxb-"+getClassName()+"."+typeFormatName);
        @NotNull final Data data =(Data) unmarshaller.unmarshal(file);

        try {
            JavaToMysql.begin();
            if (data.getProjectMap().size() > 0)
                getServiceLocator().getProjectService().setProjectMapRepository( new MyMap(data.getProjectMap()) );
            if (data.getTaskMap().size() > 0)
                getServiceLocator().getTaskService().setTaskMapRepository( new MyMap(data.getTaskMap()) );
            JavaToMysql.getConnection().commit();
        }catch (Exception e){e.printStackTrace(); try { JavaToMysql.getConnection().rollback(); } catch (Exception e1){}}
        finally {JavaToMysql.close();}
    }

    @Override
    public void saveFasterXml() throws Exception {
        @NotNull DataXml dataXml = new DataXml();
        dataXml.getListKeyProject().addAll(getProjectMapRepository().getMap().keySet());
        dataXml.getListValueProject().addAll((Collection<? extends Project>) getProjectMapRepository().getMap().values());
        dataXml.getListKeyTask().addAll(getServiceLocator().getTaskService().getTaskMapRepository().getMap().keySet());
        dataXml.getListValueTask().addAll((Collection<? extends Task>) getServiceLocator().getTaskService().getTaskMapRepository().getMap().values());

        @NotNull final File file = new File("data-faster-"+getClassName()+".xml");
        @NotNull final XmlMapper  mapper = new XmlMapper();
        mapper.configure(ToXmlGenerator.Feature.WRITE_XML_DECLARATION, true);
        mapper.writerWithDefaultPrettyPrinter().writeValue(file, dataXml);
    }
    @Override
    public void loadFasterXml() throws Exception {
        @NotNull final File file = new File("data-faster-"+getClassName()+".xml");
        @NotNull final XmlMapper mapper = new XmlMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        @NotNull final DataXml data = mapper.readValue(file, DataXml.class);

        @NotNull Map<String, Project> mapProject = new HashMap<>();
        @NotNull Map<String, Task> mapTask       = new HashMap<>();
        try {
                JavaToMysql.begin();
                if ( (data.getListKeyProject().size() > 0) && (data.getListValueProject().size() > 0) ){
                for (int i = 0; i < data.getListKeyProject().size(); i++) {
                    mapProject.put(data.getListKeyProject().get(i), data.getListValueProject().get(i));
                }
                getServiceLocator().getProjectService().setProjectMapRepository( new MyMap(mapProject) );
            }

            if ( (data.getListKeyTask().size() > 0) && (data.getListValueTask().size() > 0) ){
                for (int i = 0; i < data.getListKeyTask().size(); i++) {
                    mapTask.put(data.getListKeyTask().get(i), data.getListValueTask().get(i));
                }
                getServiceLocator().getTaskService().setTaskMapRepository( new MyMap(mapTask) );
            }
            JavaToMysql.getConnection().commit();
        }catch (Exception e){e.printStackTrace(); try { JavaToMysql.getConnection().rollback(); } catch (Exception e1){}}
        finally {JavaToMysql.close();}
    }

    @Override
    public void saveFasterJson() throws Exception {
        @NotNull final Data data = new Data() ;
        data.setProjectMap( (Map<String, Project>)getProjectMapRepository().getMap() );
        data.setTaskMap( (Map<String, Task>) getServiceLocator().getTaskService().getTaskMapRepository().getMap() );

        @NotNull final File file = new File("data-faster-"+getClassName()+".json");
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        @NotNull final String s = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(data);
        FileOutputStream outputStream = new FileOutputStream(file);
        outputStream.write(s.getBytes());
        getServiceLocator().getTerminalService().printMassageCompleted();
    }
    @Override
    public void loadFasterJson() throws Exception {
        @NotNull final File file = new File("data-faster-"+getClassName()+".json");
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        @NotNull final Data data = mapper.readValue(file, Data.class);
        try {
                JavaToMysql.begin();
                if (data.getProjectMap().size() > 0) {
                getRepository().getMapRepository().clear();
                getRepository().getMapRepository().putAll( data.getProjectMap() );
            }
            if (data.getTaskMap().size() > 0){
                getServiceLocator().getTaskService().getTaskMapRepository().getMap().clear();
                getServiceLocator().getTaskService().setTaskMapRepository( new MyMap(data.getTaskMap()) );
            }
            JavaToMysql.getConnection().commit();
        }catch (Exception e){e.printStackTrace(); try { JavaToMysql.getConnection().rollback(); } catch (Exception e1){}}
        finally {JavaToMysql.close();}
    }
}

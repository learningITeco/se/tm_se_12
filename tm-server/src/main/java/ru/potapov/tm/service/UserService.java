package ru.potapov.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.api.IRepository;
import ru.potapov.tm.api.IUserService;
import ru.potapov.tm.api.ServiceLocator;
import ru.potapov.tm.dto.MyMap;
import ru.potapov.tm.endpoint.AbstractServiceEndpoint;
import ru.potapov.tm.endpoint.IUserEndpoint;
import ru.potapov.tm.entity.RoleType;
import ru.potapov.tm.entity.Session;
import ru.potapov.tm.entity.User;
import ru.potapov.tm.util.JavaToMysql;
import ru.potapov.tm.util.SignatureUtil;
import ru.potapov.tm.util.ValidateExeption;

import javax.jws.WebService;
import java.io.*;
import java.util.*;

@Getter
@Setter
@NoArgsConstructor
@WebService(endpointInterface = "ru.potapov.tm.endpoint.IUserEndpoint")
public final class UserService extends AbstractService<User> implements IUserService {
    @Nullable
    private User authorizedUser = null;
    private boolean isAuthorized = false;

    public UserService(@NotNull ServiceLocator serviceLocator, @NotNull IRepository<User> repository) {
        super(serviceLocator, repository);
    }

    @Override
    public @Nullable RoleType getUserRole(@NotNull User user) throws ValidateExeption {
        @Nullable RoleType roleType = RoleType.User;
        try {
            JavaToMysql.begin();
            roleType = user.getRoleType();
            JavaToMysql.getConnection().commit();
        }catch (Exception e){e.printStackTrace(); try { JavaToMysql.getConnection().rollback(); } catch (Exception e1){}}
        finally {JavaToMysql.close();}
        return roleType;
    }

    @Override
    public boolean isAdministrator(@NotNull User user) throws ValidateExeption {
        if (getUserRole(user).equals(RoleType.Administrator))
            return true;

        return false;
    }

    @Override
    public @Nullable User getUserByNamePass(@NotNull final String name, @NotNull final String pass) throws Exception {
        @NotNull User user = getUserByName(name);
        if (Objects.isNull(user)) {
            return null;
        }

        @NotNull final String hashPass = SignatureUtil.sign(pass, "", 1);

        if (!isUserPassCorrect(user, hashPass)) {
            getServiceLocator().getTerminalService().printlnArbitraryMassage("Failed the user password!");
            return null;
        }

        @NotNull Session session = new Session();
        session.setUserId(user.getId());
        getServiceLocator().getSessionService().generateSession(session, user);
        getServiceLocator().getSessionService().addSession(session, user.getId());
        user.setSession(session);
        return user;
    }

    @Override
    public void setUserMapRepository(@NotNull MyMap mapRepository) {
        try {
            JavaToMysql.begin();
            getRepository().setMapRepository((Map<String, User>) mapRepository.getMap());
            JavaToMysql.getConnection().commit();
        }catch (Exception e){e.printStackTrace(); try { JavaToMysql.getConnection().rollback(); } catch (Exception e1){}}
        finally {JavaToMysql.close();}
    }

    @Override
    public void saveAllUsers() throws Exception {
        @NotNull final File file = new File("users.bin");
        @NotNull final ObjectOutputStream inputStream = new ObjectOutputStream(new FileOutputStream(file));
        @NotNull Map<String, User> mapObj = new HashMap<>();
        try {
            JavaToMysql.begin();
            mapObj = (Map<String, User>) getUserMapRepository().getMap();
            JavaToMysql.getConnection().commit();
        }catch (Exception e){e.printStackTrace(); try { JavaToMysql.getConnection().rollback(); } catch (Exception e1){}}
        finally {JavaToMysql.close();}
        inputStream.writeObject(mapObj);
    }

    @Override
    public int sizeUserMap() {
        return getUserCollection().size();
    }

    @Override
    public void loadAllUsers() throws Exception {
        @NotNull final File file = new File("users.bin");
        if (!file.canRead()) {
            //getBootstrap().getTerminalService().printlnArbitraryMassage("File cannot be opened");
            return;
        }

        @NotNull final ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(file));
        @NotNull final Object objMapUser = inputStream.readObject();
        if (objMapUser instanceof Map) {
            @NotNull final Map<String, User> mapUser = (Map<String, User>) objMapUser;
            try {
                JavaToMysql.begin();
                setUserMapRepository(new MyMap(mapUser));
                JavaToMysql.getConnection().commit();
            }catch (Exception e){e.printStackTrace(); try { JavaToMysql.getConnection().rollback(); } catch (Exception e1){}}
            finally {JavaToMysql.close();}
        }
    }

    @Override
    public @NotNull MyMap getUserMapRepository() {
        @NotNull MyMap myMap = new MyMap();
        try {
            JavaToMysql.begin();
            myMap =  new MyMap(getRepository().getMapRepository());
            JavaToMysql.getConnection().commit();
        }catch (Exception e){e.printStackTrace(); try { JavaToMysql.getConnection().rollback(); } catch (Exception e1){}}
        finally {JavaToMysql.close();}
        return myMap;
    }

    @NotNull
    @Override
    public User createUser(@Nullable final String name, @Nullable final String hashPass, @Nullable final RoleType role) {
        @NotNull User user = new User();
        user.setRoleType(role);
        user.setLogin(name);
        user.setHashPass(hashPass);
        user.setId(UUID.randomUUID().toString());

        try {
            JavaToMysql.begin();
            getRepository().persist(user);
            JavaToMysql.getConnection().commit();
        }catch (Exception e){e.printStackTrace(); try { JavaToMysql.getConnection().rollback(); } catch (Exception e1){}}
        finally {JavaToMysql.close();}

        return user;
    }

    @Nullable
    @Override
    public User getUserByName(@Nullable final String name) {
        @Nullable User user = null;
        try {
            JavaToMysql.begin();
            user = getRepository().findOne(name);
            JavaToMysql.getConnection().commit();
        }catch (Exception e){e.printStackTrace(); try { JavaToMysql.getConnection().rollback(); } catch (Exception e1){}}
        finally {JavaToMysql.close();}

        return user;
    }

    @Nullable
    @Override
    public User getUserById(@Nullable final String id) throws ValidateExeption {
        @Nullable User user = null;
        try {
            JavaToMysql.begin();
            user =  getRepository().findOneById(id);
            JavaToMysql.getConnection().commit();
        }catch (Exception e){e.printStackTrace(); try { JavaToMysql.getConnection().rollback(); } catch (Exception e1){}}
        finally {JavaToMysql.close();}

        return user;
    }

    @Override
    public boolean isUserPassCorrect(@Nullable final User user, @Nullable final String hashPass) {
        boolean res = false;
        if (user.getHashPass().equals(hashPass))
            res = true;

        return res;
    }

    @NotNull
    @Override
    public Collection<User> getUserCollection() {
        @NotNull Collection<User> list = new ArrayList<>();
        try {
            JavaToMysql.begin();
            list = getRepository().findAll();
            JavaToMysql.getConnection().commit();
        }catch (Exception e){e.printStackTrace(); try { JavaToMysql.getConnection().rollback(); } catch (Exception e1){}}
        finally {JavaToMysql.close();}
        return list;
    }

    @NotNull
    @Override
    public User changePass(@Nullable User user, @Nullable final String newHashPass) throws CloneNotSupportedException, ValidateExeption {
        @NotNull User newUser = (User) user.clone();
        newUser.setHashPass(newHashPass);
        try {
            JavaToMysql.begin();
            user = getRepository().merge(newUser);
            JavaToMysql.getConnection().commit();
        }catch (Exception e){e.printStackTrace(); try { JavaToMysql.getConnection().rollback(); } catch (Exception e1){}}
        finally {JavaToMysql.close();}
        return user;
    }

    @Override
    public void putUser(@Nullable final User user) throws ValidateExeption {
        try {
            JavaToMysql.begin();
            getRepository().persist(user);
            JavaToMysql.getConnection().commit();
        }catch (Exception e){e.printStackTrace(); try { JavaToMysql.getConnection().rollback(); } catch (Exception e1){}}
        finally {JavaToMysql.close();}
    }

    @NotNull
    @Override
    public String collectUserInfo(@Nullable final User user) throws ValidateExeption {
        @NotNull String res = "";
        res += "\n";
        res += "    User [" + user.getLogin() + "]" + "\n";
        res += "    ID: " + user.getId() + "\n";
        res += "    Role: " + user.getRoleType() + "\n";

        return res;
    }

    @Override
    public void createPredefinedUsers() {
        @NotNull String hashPass;

        hashPass = SignatureUtil.sign("1", "", 1);
        createUser("user", hashPass, RoleType.User);

        hashPass = SignatureUtil.sign("1", "", 1);
        createUser("user2", hashPass, RoleType.User);

        hashPass = SignatureUtil.sign("2", "", 1);
        createUser("admin", hashPass, RoleType.Administrator);
    }
}

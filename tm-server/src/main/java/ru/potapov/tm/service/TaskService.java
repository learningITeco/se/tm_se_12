package ru.potapov.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.api.IRepository;
import ru.potapov.tm.api.ITaskRepository;
import ru.potapov.tm.api.ITaskService;
import ru.potapov.tm.api.ServiceLocator;
import ru.potapov.tm.dto.MyMap;
import ru.potapov.tm.endpoint.AbstractServiceEndpoint;
import ru.potapov.tm.endpoint.ITaskEndpoint;
import ru.potapov.tm.entity.Project;
import ru.potapov.tm.entity.Session;
import ru.potapov.tm.entity.Task;
import ru.potapov.tm.util.JavaToMysql;
import ru.potapov.tm.util.ValidateExeption;

import javax.jws.WebService;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
@WebService(endpointInterface = "ru.potapov.tm.endpoint.ITaskEndpoint")
public final class TaskService extends AbstractService<Task> implements ITaskService {
    public TaskService(@NotNull ServiceLocator serviceLocator, @NotNull IRepository<Task> repository) {
        super(serviceLocator, repository);
    }

    @Override
    public int checkTaskSize() throws ValidateExeption{
        if (Objects.isNull(getRepository()))
            return 0;
        int i = 0;
        try {
            JavaToMysql.begin();
            i =  getRepository().getCollection().size();
            JavaToMysql.getConnection().commit();
        }catch (Exception e){e.printStackTrace(); try { JavaToMysql.getConnection().rollback(); } catch (Exception e1){}}
        finally {JavaToMysql.close();}
        return i;
    }

    @Nullable
    @Override
    public Task findTaskByName(@NotNull final String name) throws ValidateExeption{
        @Nullable Task task = null;
        if (Objects.isNull(getRepository()))
            return task;

        try {
            JavaToMysql.begin();
            task = getRepository().findOne(name);
            JavaToMysql.getConnection().commit();
        }catch (Exception e){e.printStackTrace(); try { JavaToMysql.getConnection().rollback(); } catch (Exception e1){}}
        finally {JavaToMysql.close();}
        return task;
    }

    @Override
    public void removeTask(@NotNull final Task task) throws ValidateExeption{
        if (Objects.isNull(getRepository()))
            return;

        try {
            JavaToMysql.begin();
            getRepository().remove(task);
            JavaToMysql.getConnection().commit();
        }catch (Exception e){e.printStackTrace(); try { JavaToMysql.getConnection().rollback(); } catch (Exception e1){}}
        finally {JavaToMysql.close();}
    }

    @Override
    public void removeAllTasksByUserId(@NotNull final String userId) throws ValidateExeption{
        if (Objects.isNull(getRepository()))
            return;
        try {
            JavaToMysql.begin();
            getRepository().removeAll(userId);
            JavaToMysql.getConnection().commit();
        }catch (Exception e){e.printStackTrace(); try { JavaToMysql.getConnection().rollback(); } catch (Exception e1){}}
        finally {JavaToMysql.close();}
    }

    @Override
    public void removeAllTasks(@NotNull final Collection<Task> listTasks) throws ValidateExeption{
        if (Objects.isNull(getRepository()))
            return;
        try {
            JavaToMysql.begin();
            getRepository().removeAll(listTasks);
            JavaToMysql.getConnection().commit();
        }catch (Exception e){e.printStackTrace(); try { JavaToMysql.getConnection().rollback(); } catch (Exception e1){}}
        finally {JavaToMysql.close();}
    }

    @NotNull
    @Override
    public Task renameTask(@NotNull final Task task, @NotNull final String name) throws CloneNotSupportedException, ValidateExeption{
        if (Objects.nonNull(name) && Objects.nonNull(getRepository())){
            @NotNull Task newTask = (Task) task.clone();
            newTask.setName(name);
            try {
                JavaToMysql.begin();
                getRepository().merge(newTask);
                JavaToMysql.getConnection().commit();
            }catch (Exception e){e.printStackTrace(); try { JavaToMysql.getConnection().rollback(); } catch (Exception e1){}}
            finally {JavaToMysql.close();}
        }
        return task;
    }

    @Override
    public void changeProject(@NotNull final Task task, @Nullable final Project project) throws CloneNotSupportedException, ValidateExeption{
        if (Objects.nonNull(project) && Objects.nonNull(getRepository())){
            @NotNull Task newTask = (Task) task.clone();
            newTask.setProjectId(project.getId());
            try {
                JavaToMysql.begin();
                getRepository().merge(newTask);
                JavaToMysql.getConnection().commit();
            }catch (Exception e){e.printStackTrace(); try { JavaToMysql.getConnection().rollback(); } catch (Exception e1){}}
            finally {JavaToMysql.close();}
        }
    }

    @NotNull
    @Override
    public Collection<Task> findAllTasksByUserId(@NotNull final String userId, @NotNull final String projectId) throws ValidateExeption{
        if (Objects.isNull(getRepository()))
            return new ArrayList<>();

        @NotNull Collection<Task> list = new ArrayList<>();
        try {
            JavaToMysql.begin();
            list =  ((ITaskRepository)getRepository()).findAll(userId, projectId);
            JavaToMysql.getConnection().commit();
        }catch (Exception e){e.printStackTrace(); try { JavaToMysql.getConnection().rollback(); } catch (Exception e1){}}
        finally {JavaToMysql.close();}
        return list;
    }

    @Override
    public @NotNull Collection<Task> findAllByUser(String userId) throws ValidateExeption {
        if (Objects.isNull(getRepository()))
            return new ArrayList<>();

        @NotNull Collection<Task> list = new ArrayList<>();
        try {
            JavaToMysql.begin();
            list = ((ITaskRepository) getRepository()).findAllByUser(userId);
            JavaToMysql.getConnection().commit();
        }catch (Exception e){e.printStackTrace(); try { JavaToMysql.getConnection().rollback(); } catch (Exception e1){}}
        finally {JavaToMysql.close();}
        return list;
    }

    @NotNull
    @Override
    public Collection<Task> findAllTasks(@NotNull final String projectId) throws ValidateExeption{
        if (Objects.isNull(getRepository()))
            return new ArrayList<>();

        @NotNull Collection<Task> list = new ArrayList<>();
        try {
            JavaToMysql.begin();
            list =  getRepository().findAll();
            JavaToMysql.getConnection().commit();
        }catch (Exception e){e.printStackTrace(); try { JavaToMysql.getConnection().rollback(); } catch (Exception e1){}}
        finally {JavaToMysql.close();}
        return list;
    }

    @Override
    public void putTask(@NotNull final Task task) throws ValidateExeption{
        if (Objects.isNull(getRepository()))
            return;

        try {
            JavaToMysql.begin();
            if (Objects.isNull( getRepository().findOneById(task.getId())  ))
                getRepository().persist(task);
            else
                getRepository().merge(task);
            JavaToMysql.getConnection().commit();
        }catch (Exception e){e.printStackTrace(); try { JavaToMysql.getConnection().rollback(); } catch (Exception e1){}}
        finally {JavaToMysql.close();}
    }

    @Override
    public @NotNull MyMap getTaskMapRepository() {
        @NotNull MyMap myMap = new MyMap();
        try {
            JavaToMysql.begin();
            myMap = new MyMap(getRepository().getMapRepository());
            JavaToMysql.getConnection().commit();
        }catch (Exception e){e.printStackTrace(); try { JavaToMysql.getConnection().rollback(); } catch (Exception e1){}}
        finally {JavaToMysql.close();}
        return myMap;
    }

    @Override
    public void loadBinar() throws Exception {
    }

    @Override
    public void saveBinar() throws Exception {
    }

    @Override
    public void setTaskMapRepository(@NotNull final MyMap mapRepository){
        try {
            JavaToMysql.begin();
            getRepository().setMapRepository((Map<String, Task>) mapRepository.getMap());
            JavaToMysql.getConnection().commit();
        }catch (Exception e){e.printStackTrace(); try { JavaToMysql.getConnection().rollback(); } catch (Exception e1){}}
        finally {JavaToMysql.close();}
    };

    @NotNull
    @Override
    public String collectTaskInfo(@NotNull final Task task) throws ValidateExeption {
        @NotNull String res = "";
        if (Objects.isNull(task.getDateStart()) || Objects.isNull(task.getDateFinish()) || Objects.isNull(getServiceLocator())
       )
            return res;

        res += "\n";
        res += "    Task [" + task.getName() + "]" +  "\n";
        res += "    Project " + getServiceLocator().getProjectService().findOneProjectById(task.getProjectId())  + "]" +  "\n";
        res += "    Status: " + task.getStatus() + "\n";
        res += "    Description: " + task.getDescription() + "\n";
        res += "    ID: " + task.getId() + "\n";
        res += "    Date start: " +  getServiceLocator().getFt().format(task.getDateStart() ) + "\n";
        res += "    Date finish: " + getServiceLocator().getFt().format(task.getDateFinish() ) + "\n";

        return res;
    }
    //Save-Load
    @Override public void saveJaxb(boolean formatXml) throws Exception { }
    @Override public void loadJaxb(boolean formatXml) throws Exception {}

    @Override public void saveFasterXml() throws Exception { }
    @Override public void loadFasterXml() throws Exception {}

    @Override public void saveFasterJson() throws Exception { }
    @Override public void loadFasterJson() throws Exception {}
}

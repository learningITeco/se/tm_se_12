package ru.potapov.tm.util;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.*;
import java.util.Collection;

@Getter
@Setter
@NoArgsConstructor
public class JavaToMysql {
    //connection
    @NotNull private static final String url        = "jdbc:mysql://localhost:3306/tm";
    @NotNull private static final String user       = "root";
    @NotNull private static final String password   = "root";

    //state
    @NotNull private static Connection          connection;
    @NotNull private static Statement           statement;
    @NotNull private static PreparedStatement   preparedStatement;
    @NotNull private static ResultSet           resultSet;

    @Nullable
    public ResultSet executeQuery(String query) {
        try {
            connection  = DriverManager.getConnection(JavaToMysql.url, JavaToMysql.user, JavaToMysql.password);
            connection.setAutoCommit(false);
            statement   = connection.createStatement();
            resultSet = statement.executeQuery(query);
            connection.commit();
        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
            try{
                if(connection!=null)
                    connection.rollback();
            }catch(SQLException se2){ se2.printStackTrace(); }//end try
        } finally {
            //close connection ,stmt and resultset here
            //try { connection.close(); } catch(SQLException se) { /*can't do anything */ }
            //try { statement.close(); } catch(SQLException se) { /*can't do anything */ }
            //try { resultSet.close(); } catch(SQLException se) { /*can't do anything */ }
        }
        return resultSet;
    }

    public static void close(){
        //close connection ,stmt and resultset here
        try { connection.close(); } catch(SQLException se) { /*can't do anything */ }
        try { statement.close(); } catch(SQLException se) { /*can't do anything */ }
        try { resultSet.close(); } catch(SQLException se) { /*can't do anything */ }
    }

    @Nullable
    public static void begin() {
        try {
            connection  = DriverManager.getConnection(JavaToMysql.url, JavaToMysql.user, JavaToMysql.password);
            connection.setAutoCommit(false);

            statement   = connection.createStatement();

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
            try{
                if(connection!=null)
                    connection.rollback();
            }catch(SQLException se2){ se2.printStackTrace(); }//end try
            try { connection.close(); } catch(SQLException se) { /*can't do anything */ }
            try { statement.close(); } catch(SQLException se) { /*can't do anything */ }
            try { resultSet.close(); } catch(SQLException se) { /*can't do anything */ }
        }
    }

    @SneakyThrows
    public PreparedStatement prepareStatement(String query){
        connection          = DriverManager.getConnection(JavaToMysql.url, JavaToMysql.user, JavaToMysql.password);
        connection.setAutoCommit(false);
        preparedStatement   = connection.prepareStatement(query);
        return preparedStatement;
    }

    public static void setConnection(Connection connection) {
        JavaToMysql.connection = connection;
    }

    public static void setStatement(Statement statement) {
        JavaToMysql.statement = statement;
    }

    public static void setPreparedStatement(PreparedStatement preparedStatement) {
        JavaToMysql.preparedStatement = preparedStatement;
    }

    public static Connection getConnection() {
        return connection;
    }

    public static Statement getStatement() {
        return statement;
    }

    public static PreparedStatement getPreparedStatement() {
        return preparedStatement;
    }

    public static ResultSet getResultSet() {
        return resultSet;
    }

    public static void main(String[] args) {
        String query =  "SELECT * FROM app_user";

        try {
            connection  = DriverManager.getConnection(JavaToMysql.url, JavaToMysql.user, JavaToMysql.password);
            statement   = connection.createStatement();
            resultSet = statement.executeQuery(query);


            while (resultSet.next()) {
                int count = resultSet.getInt(1);
                System.out.println("Total number users in the table : " + count);
            }

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        } finally {
            //close connection ,stmt and resultset here
            try { connection.close(); } catch(SQLException se) { /*can't do anything */ }
            try { statement.close(); } catch(SQLException se) { /*can't do anything */ }
            try { resultSet.close(); } catch(SQLException se) { /*can't do anything */ }
        }
    }
}

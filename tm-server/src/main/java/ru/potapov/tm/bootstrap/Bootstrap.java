package ru.potapov.tm.bootstrap;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.api.*;

import ru.potapov.tm.endpoint.*;
import ru.potapov.tm.repository.*;
import ru.potapov.tm.service.*;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Scanner;
import javax.xml.ws.Endpoint;

import lombok.Getter;
import lombok.Setter;
import ru.potapov.tm.util.JavaToMysql;

@Getter
@Setter
public final class  Bootstrap implements ServiceLocator {
    @NotNull private JavaToMysql javaToMysql = new JavaToMysql();
    @NotNull private final IProjectRepository projectRepository= new ProjectRepository(javaToMysql);
    @NotNull private final ITaskRepository taskRepository      = new TaskRepository(javaToMysql);
    @NotNull private final IUserRepository userRepository      = new UserRepository(javaToMysql);
    @NotNull private final ISessionRepository sessionRepository= new SessionRepository(javaToMysql);

    @Nullable private IProjectService projectService = new ProjectService(this, projectRepository);
    @Nullable private ITaskService taskService = new TaskService(this, taskRepository);
    @Nullable private IUserService userService = new UserService(this, userRepository);
    @Nullable private ISessionService sessionService = new SessionService(this, sessionRepository);


    @Nullable private ITerminalService terminalService = new TerminalService(this);
    @Nullable private IProjectEndpoint projectIEndpoint = new ProjectEndpoint(this);
    @Nullable private ITaskEndpoint taskIEndpoint = new TaskEndpoint(this);
    @Nullable private IUserEndpoint userIEndpoint = new UserEndpoint(this);
    @Nullable private ISessionEndpoint sessionIEndpoint = new SessionEndpoint(this);
    @Nullable private Endpoint projectEndpoint, taskEndpoint, userEndpoint, sessionEndpoint;

    @NotNull final SimpleDateFormat ft            = new SimpleDateFormat("dd-MM-yyyy");

    public Bootstrap() throws SQLException {
    }

    public void init() throws Exception{
        if (getUserService().sizeUserMap() == 0)
            getUserService().createPredefinedUsers();

        startEndpoints();

        String command = "";  Scanner in = new Scanner(System.in);
        while (!"exit".equals(command)){
            System.out.println("Commands: <stop>, <start>, <exit>");
            command = in.nextLine();

            switch (command){
                case "start": try {
                    startEndpoints();
                } catch ( Exception e) { e.printStackTrace(); }
                finally { break; }
                case "stop": stopEndpoints();break;
            }
        }
        stopEndpoints();
    }

    private void startEndpoints() {
        projectEndpoint =  Endpoint.publish("http://localhost:8080/ProjectEndpoint?wsdl", projectIEndpoint);
        System.out.println("http://localhost:8080/ProjectEndpoint?wsdl");

        taskEndpoint    = Endpoint.publish("http://localhost:8080/TaskEndpoint?wsdl", taskIEndpoint);
        System.out.println("http://localhost:8080/TaskEndpoint?wsdl");

        userEndpoint    = Endpoint.publish("http://localhost:8080/UserEndpoint?wsdl", userIEndpoint);
        System.out.println("http://localhost:8080/UserEndpoint?wsdl");

        sessionEndpoint = Endpoint.publish("http://localhost:8080/SessionEndpoint?wsdl", sessionIEndpoint);
        System.out.println("http://localhost:8080/SessionEndpoint?wsdl");
        System.out.println("Command <start> completed");
    }

    private void stopEndpoints() {
        projectEndpoint.stop();
        System.out.println("http://localhost:8080/ProjectEndpoint?wsdl - has stopped");

        taskEndpoint.stop();
        System.out.println("http://localhost:8080/TaskEndpoint?wsdl - has stopped");

        userEndpoint.stop();
        System.out.println("http://localhost:8080/UserEndpoint?wsdl - has stopped");

        sessionEndpoint.stop();
        System.out.println("http://localhost:8080/SessionEndpoint?wsdl - has stopped");
        System.out.println("Command <stop> completed");
    }

    @NotNull
    @Override
    public ITerminalService getTerminalService() {
        return terminalService;
    }

    @NotNull
    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @NotNull
    @Override
    public IUserService getUserService() {
        return userService;
    }

    @NotNull
    @Override
    public ISessionService getSessionService(){
        return sessionService;
    }
}
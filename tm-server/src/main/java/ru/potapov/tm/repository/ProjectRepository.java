package ru.potapov.tm.repository;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.api.IProjectRepository;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import lombok.Getter;
import lombok.Setter;
import ru.potapov.tm.entity.Project;
import ru.potapov.tm.entity.RoleType;
import ru.potapov.tm.entity.User;
import ru.potapov.tm.util.JavaToMysql;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository<Project> {
    public ProjectRepository(@NotNull JavaToMysql javaToMysql) {
        super(javaToMysql);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findOneById(@NotNull String id) {
        @NotNull final List<Project> result = new ArrayList<>();
        @NotNull final String query = "SELECT * FROM app_project WHERE id = ?";
        @NotNull PreparedStatement preparedStatement = getJavaToMysql().prepareStatement(query);
        preparedStatement.setString(1, id);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()){
            Project project = fetch(resultSet);
            resultSet.close();
            return project;
        }
        resultSet.close();
        return null;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Collection<Project> findAll() {
        return getListProjects();
    }

    @NotNull
    @Override
    @SneakyThrows
    public Collection<Project> findAll(@NotNull final String userId) {
        @NotNull final List<Project> result = new ArrayList<>();
        @NotNull final String query = "SELECT * FROM app_project WHERE user_id = ?";
        @NotNull PreparedStatement preparedStatement = getJavaToMysql().prepareStatement(query);
        preparedStatement.setString(1, userId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) result.add(fetch(resultSet));
        resultSet.close();
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findOne(@NotNull final String name) {
        @NotNull final String query = "SELECT * FROM app_project WHERE name = ?";
        @NotNull PreparedStatement preparedStatement = getJavaToMysql().prepareStatement(query);
        preparedStatement.setString(1, name);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();

        while (resultSet.next()) {
            Project project = fetch(resultSet);
            preparedStatement.close();
            return project;
        }
        preparedStatement.close();
        return null;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findOne(@NotNull final String userId, @NotNull final String name) {
        @NotNull final String query = "SELECT * FROM app_project WHERE name = ? AND user_id = ?";
        @NotNull PreparedStatement preparedStatement = getJavaToMysql().prepareStatement(query);
        preparedStatement.setString(1, name);
        preparedStatement.setString(2, userId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();

        while (resultSet.next()) {
            Project project = fetch(resultSet);
            preparedStatement.close();
            return project;        }
        preparedStatement.close();
        return null;
    }

    @Override
    @SneakyThrows
    public void persist(@NotNull final Project project) {
        String query = "INSERT INTO app_project (name, description, user_id, dateBegin, dateEnd, id)  VALUES (?, ?, ?, ?, ?, ?) ";
        PreparedStatement preparedStatement = getJavaToMysql().prepareStatement(query);
        preparedStatement.setString(1, project.getName());
        preparedStatement.setString(2, project.getDescription());
        preparedStatement.setString(3, project.getUserId());
        preparedStatement.setDate(4, new Date(project.getDateStart().getTime()));
        preparedStatement.setDate(5, new Date(project.getDateFinish().getTime()));
        preparedStatement.setString(6, project.getId());
        preparedStatement.execute();
        preparedStatement.close();
    }

    @Override
    @SneakyThrows
    public void persist(@NotNull final String userId, @NotNull final Project project) {
        if (userId.equals(project.getUserId()))
            persist(project);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project merge(@NotNull final Project project) {
        String query = "UPDATE app_project SET name=?, description=?, user_id=?, dateBegin=?, dateEnd=? WHERE id = ?";
        PreparedStatement preparedStatement = getJavaToMysql().prepareStatement(query);
        preparedStatement.setString(1, project.getName());
        preparedStatement.setString(2, project.getDescription());
        preparedStatement.setString(3, project.getUserId());
        preparedStatement.setDate(4, new Date(project.getDateStart().getTime()));
        preparedStatement.setDate(5, new Date(project.getDateFinish().getTime()));
        preparedStatement.setString(6, project.getId());
        preparedStatement.executeUpdate();
        preparedStatement.close();
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project merge(@NotNull final String userId, @NotNull final Project project) {
        if (userId.equals(project.getUserId()))
            return merge(project);
        return project;
    }

    @Override
    @SneakyThrows
    public void remove(@NotNull final Project project) {
        String query = "DELETE FROM app_project WHERE  id = ? ";
        PreparedStatement preparedStatement = getJavaToMysql().prepareStatement(query);
        preparedStatement.setString(1, project.getId());
        preparedStatement.execute();
        preparedStatement.close();
    }

    @Override
    @SneakyThrows
    public void remove(@NotNull final String userId, @NotNull final Project project) {
        if (!userId.equals(project.getUserId()))
            return;

        remove(project);
    }

    @Override
    @SneakyThrows
    public void removeAll() {
        String query = "DELETE * FROM app_project";
        PreparedStatement preparedStatement = getJavaToMysql().prepareStatement(query);
        preparedStatement.execute();
        preparedStatement.close();
    }

    @Override
    @SneakyThrows
    public void removeAll(@NotNull final String userId) {
        for (Project project : findAll(userId)) {
            if (userId.equals(project.getUserId()))
                remove(project);
        }
    }

    @Override
    @SneakyThrows
    public void removeAll(@NotNull final Collection<Project> list) {
        for (Project project : list) {
            remove(project);
        }
    }

    @Override
    @SneakyThrows
    public void removeAll(@NotNull final String userId, @NotNull final Collection<Project> list) {
        for (Project project : list) {
            if (project.getUserId().equals(userId))
                remove(project);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Collection<Project> getCollection(){
        return getListProjects();
    }

    @NotNull
    @Override
    @SneakyThrows
    public Collection<Project> getCollection(@NotNull final String userId){
        Collection<Project> collections = new ArrayList<>();
        @NotNull final String query = "SELECT * FROM app_project WHERE id = ?";
        @NotNull PreparedStatement preparedStatement = getJavaToMysql().prepareStatement(query);
        preparedStatement.setString(1, userId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();

        while (resultSet.next()) collections.add(fetch(resultSet));
        preparedStatement.close();

        return collections;
    }

    @Nullable
    @SneakyThrows
    private Project fetch(@Nullable final ResultSet row) {
        if (row == null) return null;
        @NotNull final Project project = new Project();
        project.setId(row.getString("id"));
        project.setName(row.getString("name"));
        project.setDescription(row.getString("description"));
        project.setUserId(row.getString("user_id"));
        project.setDateStart(row.getDate("dateBegin"));
        project.setDateFinish(row.getDate("dateEnd"));

        return project;
    }

    @NotNull
    @SneakyThrows
    public List<Project> getListProjects() {
        @NotNull final List<Project> result = new ArrayList<>();
        @NotNull final String query = "SELECT * FROM app_project";
        @NotNull final ResultSet resultSet = getJavaToMysql().executeQuery(query);
        while (resultSet.next()) result.add(fetch(resultSet));
        resultSet.close();
        return result;
    }
}

package ru.potapov.tm.repository;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.api.ITaskRepository;
import ru.potapov.tm.entity.Project;
import ru.potapov.tm.entity.Task;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;

import lombok.Getter;
import lombok.Setter;
import ru.potapov.tm.util.JavaToMysql;

@Getter
@Setter
@NoArgsConstructor
public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository{
    public TaskRepository(@NotNull JavaToMysql javaToMysql) {
        super(javaToMysql);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Collection findAll() {
        return getListTasks();
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findOneById(@NotNull String id) {
        @NotNull final List<Project> result = new ArrayList<>();
        @NotNull final String query = "SELECT * FROM app_task WHERE id = ?";
        @NotNull PreparedStatement preparedStatement = getJavaToMysql().prepareStatement(query);
        preparedStatement.setString(1, id);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()){
            Task project = fetch(resultSet); resultSet.close(); return project;
        }
        resultSet.close();
        return null;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findOne(@NotNull final String name) {
        @NotNull final String query = "SELECT * FROM app_task WHERE name = ?";
        @NotNull PreparedStatement preparedStatement = getJavaToMysql().prepareStatement(query);
        preparedStatement.setString(1, name);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();

        while (resultSet.next()) {
            Task task = fetch(resultSet);
            preparedStatement.close();
            return task;
        }
        preparedStatement.close();
        return null;
    }

    @Override
    @SneakyThrows
    public void persist(@NotNull final Task task) {
        String query = "INSERT INTO app_task (name, description, user_id, dateBegin, dateEnd, project_id, id)  VALUES (?, ?, ?, ?, ?, ?, ?) ";
        PreparedStatement preparedStatement = getJavaToMysql().prepareStatement(query);
        preparedStatement.setString(1, task.getName());
        preparedStatement.setString(2, task.getDescription());
        preparedStatement.setString(3, task.getUserId());
        preparedStatement.setDate(4, new Date(task.getDateStart().getTime()));
        preparedStatement.setDate(5, new Date(task.getDateFinish().getTime()));
        preparedStatement.setString(6, task.getProjectId());
        preparedStatement.setString(7, task.getId());
        preparedStatement.execute();
        preparedStatement.close();
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task merge(@NotNull final Task task) {
        String query = "UPDATE app_task SET name=?, description=?, user_id=?, dateBegin=?, dateEnd=? , project_id = ? WHERE id = ?";
        PreparedStatement preparedStatement = getJavaToMysql().prepareStatement(query);
        preparedStatement.setString(1, task.getName());
        preparedStatement.setString(2, task.getDescription());
        preparedStatement.setString(3, task.getUserId());
        preparedStatement.setDate(4, new Date(task.getDateStart().getTime()));
        preparedStatement.setDate(5, new Date(task.getDateFinish().getTime()));
        preparedStatement.setString(6, task.getProjectId());
        preparedStatement.setString(7, task.getId());
        preparedStatement.execute();
        preparedStatement.close();
        return task;
    }

    @Override
    @SneakyThrows
    public void remove(@NotNull final Task task) {
        String query = "DELETE FROM app_task WHERE  id = ? ";
        PreparedStatement preparedStatement = getJavaToMysql().prepareStatement(query);
        preparedStatement.setString(1, task.getId());
        preparedStatement.execute();
        preparedStatement.close();
    }

    @Override
    @SneakyThrows
    public void removeAll() {
        String query = "DELETE * FROM app_task";
        PreparedStatement preparedStatement = getJavaToMysql().prepareStatement(query);
        preparedStatement.execute();
        preparedStatement.close();
    }

    @Override
    @SneakyThrows
    public void removeAll(@NotNull final Collection<Task> list) {
        for (Task task : list) {
            remove(task);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Collection<Task> getCollection(){
        return getListTasks();
    }

    @Override
    @SneakyThrows
    public @NotNull  Collection<Task> findAllByUser(@NotNull final String userId) {
        return findAll(userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    public  Collection<Task> findAll(@NotNull final String userId) {
        Collection<Task> collections = new ArrayList<>();
        @NotNull final String query = "SELECT * FROM app_task WHERE user_id = ?";
        @NotNull PreparedStatement preparedStatement = getJavaToMysql().prepareStatement(query);
        preparedStatement.setString(1, userId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();

        while (resultSet.next()) collections.add(fetch(resultSet));
        preparedStatement.close();

        return collections;
    }

    @NotNull
    @Override
    @SneakyThrows
    public  Collection<Task> findAll(@NotNull final String userId, @NotNull final String projectId) {
        Collection<Task> listTask = new ArrayList<>();
        @NotNull final List<Task> result = new ArrayList<>();
        @NotNull final String query = "SELECT * FROM app_task WHERE user_id = ? AND project_id = ?";
        @NotNull PreparedStatement preparedStatement = getJavaToMysql().prepareStatement(query);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, projectId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) result.add(fetch(resultSet));
        resultSet.close();

        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findOne(@NotNull final String userId, @NotNull final String name) {
        @NotNull final String query = "SELECT * FROM app_task WHERE name = ? AND user_id = ?";
        @NotNull PreparedStatement preparedStatement = getJavaToMysql().prepareStatement(query);
        preparedStatement.setString(1, name);
        preparedStatement.setString(2, userId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();

        while (resultSet.next()) {
            Task task = fetch(resultSet);
            preparedStatement.close();
            return task;        }
        preparedStatement.close();
        return null;
    }

    @Override
    @SneakyThrows
    public void persist(@NotNull final String userId, @NotNull final Task task) {
        if (userId.equals(task.getUserId()))
            persist(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task merge(@NotNull final String userId, @NotNull final Task task) {
        if (userId.equals(task.getUserId()))
            return merge(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public void remove(@NotNull final String userId, @NotNull final Task task) {
        if (!userId.equals(task.getUserId()))
            return;

        remove(task);
    }

    @Override
    @SneakyThrows
    public void removeAll(@NotNull final String userId) {
        for (Task task : findAll(userId)) {
            if (userId.equals(task.getUserId()))
                remove(task);
        }
    }

    @Override
    @SneakyThrows
    public void removeAll(@NotNull final String userId, @NotNull final Collection<Task> list) {
        for (Task task : list) {
            if (task.getUserId().equals(userId))
                remove(task);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Collection<Task> getCollection(@NotNull final String userId){
        return findAll(userId);
    }

    @Nullable
    @SneakyThrows
    private Task fetch(@Nullable final ResultSet row) {
        if (row == null) return null;
        @NotNull final Task task = new Task();
        task.setId(row.getString("id"));
        task.setProjectId(row.getString("project_id"));
        task.setName(row.getString("name"));
        task.setDescription(row.getString("description"));
        task.setUserId(row.getString("user_id"));
        task.setDateStart(row.getDate("dateBegin"));
        task.setDateFinish(row.getDate("dateEnd"));

        return task;
    }

    @NotNull
    @SneakyThrows
    public List<Task> getListTasks() {
        @NotNull final List<Task> result = new ArrayList<>();
        @NotNull final String query = "SELECT * FROM app_task";
        @NotNull final ResultSet resultSet = getJavaToMysql().executeQuery(query);
        while (resultSet.next()) result.add(fetch(resultSet));
        resultSet.close();
        return result;
    }
}

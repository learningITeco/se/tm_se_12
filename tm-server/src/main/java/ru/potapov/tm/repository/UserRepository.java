package ru.potapov.tm.repository;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.api.IUserRepository;
import ru.potapov.tm.entity.RoleType;
import ru.potapov.tm.entity.User;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import ru.potapov.tm.util.JavaToMysql;

@Getter
@Setter
@NoArgsConstructor
public final class UserRepository extends AbstractRepository<User> implements IUserRepository<User> {
    public UserRepository(@NotNull JavaToMysql javaToMysql) {
        super(javaToMysql);
    }

//    @SneakyThrows
    @NotNull
    @Override
    public Collection<User> findAll() {
        return getListUsers();
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findOne(@NotNull final String name) {
        @NotNull final String query = "SELECT * FROM app_user WHERE login = ?";
        @NotNull PreparedStatement preparedStatement = getJavaToMysql().prepareStatement(query);
        preparedStatement.setString(1, name);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();

        while (resultSet.next()){
            User user = fetch(resultSet);
            preparedStatement.close();
            return user;
        }

        preparedStatement.close();
        return null;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findOneById(@NotNull final String id) {
        @NotNull final String query = "SELECT * FROM app_user WHERE id = ?";
        @NotNull PreparedStatement preparedStatement = getJavaToMysql().prepareStatement(query);
        preparedStatement.setString(1, id);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();

        while (resultSet.next()) return fetch(resultSet);
        preparedStatement.close();

        return null;
    }

    @Override
    @SneakyThrows
    public void persist(@NotNull final User user) {
        String query = "INSERT INTO app_user  (login, passwordHash, role, id) VALUES (?, ?, ?, ?)";
        PreparedStatement preparedStatement = getJavaToMysql().prepareStatement(query);
        preparedStatement.setString(1, user.getLogin());
        preparedStatement.setString(2, user.getHashPass());
        preparedStatement.setString(3, user.getRoleType().toString());
        preparedStatement.setString(4, user.getId());
        preparedStatement.execute();
        preparedStatement.close();
    }

    @NotNull
    @Override
    @SneakyThrows
    public User merge(@NotNull final User user) {
        String query = "UPDATE app_user SET login=?, passwordHash=?, role=? WHERE id = ?";
        PreparedStatement preparedStatement = getJavaToMysql().prepareStatement(query);
        preparedStatement.setString(1, user.getLogin());
        preparedStatement.setString(2, user.getHashPass());
        preparedStatement.setString(3, user.getRoleType().toString());
        preparedStatement.setString(4, user.getId());
        preparedStatement.execute();
        preparedStatement.close();
        return user;
    }

    @Override
    @SneakyThrows
    public void remove(@NotNull final User user) {
        String query = "DELETE FROM app_user WHERE  id = ? ";
        PreparedStatement preparedStatement = getJavaToMysql().prepareStatement(query);
        preparedStatement.setString(1, user.getId());
        preparedStatement.execute();
        preparedStatement.close();
    }

    @Override
    @SneakyThrows
    public void removeAll() {
        String query = "DELETE * FROM app_user";
        PreparedStatement preparedStatement = getJavaToMysql().prepareStatement(query);
        preparedStatement.execute();
        preparedStatement.close();
    }

    @Override
    @SneakyThrows
    public void removeAll(@NotNull final Collection<User> list) {
        for (User user : list) {
            remove(user);
        }
    }

    @NotNull
    @Override
    public Collection<User> getCollection(){
        return findAll();
    }

    @Nullable
    @SneakyThrows
    private User fetch(@Nullable final ResultSet row) {
        if (row == null) return null;
        @NotNull final User user = new User();
        user.setId(row.getString("id"));
        user.setLogin(row.getString("login"));
        user.setHashPass(row.getString("passwordHash"));
        user.setRoleType(RoleType.valueOf(row.getString("role")));

        return user;
    }

    @NotNull
    @SneakyThrows
    public List<User> getListUsers() {
        @NotNull final List<User> result = new ArrayList<>();
        @NotNull final String query = "SELECT * FROM app_user";
        @NotNull final ResultSet resultSet = getJavaToMysql().executeQuery(query);
        while (resultSet.next()) result.add(fetch(resultSet));
        resultSet.close();
        return result;
    }
}

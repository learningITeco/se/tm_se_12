package ru.potapov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.api.ISessionRepository;
import ru.potapov.tm.entity.Session;
import ru.potapov.tm.util.JavaToMysql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class SessionRepository extends AbstractRepository<Session>  implements ISessionRepository<Session> {
    public SessionRepository(@NotNull JavaToMysql javaToMysql) {
        super(javaToMysql);
    }

    public SessionRepository() {
        super();
    }

    @Override
    @SneakyThrows
    public @NotNull Collection<Session> findAll() {
        return getListSessions();
    }

    @Override
    @SneakyThrows
    public @NotNull Collection<Session> findAll(String userId) {
        Collection<Session> collections = new ArrayList<>();
        @NotNull final String query = "SELECT * FROM app_session WHERE user_id = ?";
        @NotNull PreparedStatement preparedStatement = getJavaToMysql().prepareStatement(query);
        preparedStatement.setString(1, userId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();

        while (resultSet.next()) collections.add(fetch(resultSet));
        preparedStatement.close();

        return collections;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session findOne(@NotNull String id) {
        @NotNull final String query = "SELECT * FROM app_session WHERE user_id = ?";
        @NotNull PreparedStatement preparedStatement = getJavaToMysql().prepareStatement(query);
        preparedStatement.setString(1, id);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();

        while (resultSet.next()) {
            Session session = fetch(resultSet);
            preparedStatement.close();
            return session;        }
        preparedStatement.close();
        return null;    }

    @Nullable
    @Override
    @SneakyThrows
    public Session findOne(@NotNull String userId, @NotNull String id) {
        @NotNull final String query = "SELECT * FROM app_session WHERE user_id = ? AND user_id = ?";
        @NotNull PreparedStatement preparedStatement = getJavaToMysql().prepareStatement(query);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, id);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();

        while (resultSet.next()) {
            Session session = fetch(resultSet);
            preparedStatement.close();
            return session;        }
        preparedStatement.close();
        return null;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session findOneById(@NotNull String id) {
        @NotNull final String query = "SELECT * FROM app_session WHERE id = ?";
        @NotNull PreparedStatement preparedStatement = getJavaToMysql().prepareStatement(query);
        preparedStatement.setString(1, id);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();

        while (resultSet.next()) {
            Session session = fetch(resultSet);
            preparedStatement.close();
            return session;
        }
        preparedStatement.close();
        return null;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session findOneById(@NotNull String userId, @NotNull String id) {
        @NotNull final String query = "SELECT * FROM app_session WHERE id = ? AND user_id = ?";
        @NotNull PreparedStatement preparedStatement = getJavaToMysql().prepareStatement(query);
        preparedStatement.setString(1, id);
        preparedStatement.setString(1, userId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();

        while (resultSet.next()) {
            Session session = fetch(resultSet);
            preparedStatement.close();
            return session;
        }
        preparedStatement.close();
        return null;
    }

    @Override
    @SneakyThrows
    public void persist(@NotNull Session session) {
            String query = "INSERT INTO app_session(id, user_id, signature, timestamp) VALUES (?, ?, ?, ?)";
            PreparedStatement preparedStatement = getJavaToMysql().prepareStatement(query);
            preparedStatement.setString(1, session.getId());
            preparedStatement.setString(2, session.getUserId());
            preparedStatement.setString(3, session.getSignature());
            //preparedStatement.setDate(3, new Date(session.getDateStamp().getTime()));
            preparedStatement.setLong(4, session.getDateStamp().getTime());
            preparedStatement.execute();
            preparedStatement.close();
    }

    @Override
    @SneakyThrows
    public void persist(@NotNull String userId, @NotNull Session session) {
        if (userId.equals(session.getUserId()))
            persist(session);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Session merge(@NotNull Session session) {
        String query = "UPDATE app_session SET signature = ?, timestamp = ?, user_id = ? WHERE id = ?";
        PreparedStatement preparedStatement = getJavaToMysql().prepareStatement(query);
        preparedStatement.setString(1, session.getSignature());
        preparedStatement.setLong(2, session.getDateStamp().getTime());
        preparedStatement.setString(3, session.getUserId());
        preparedStatement.setString(4, session.getId());
        preparedStatement.execute();
        preparedStatement.close();
        return session;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Session merge(@NotNull String userId, @NotNull Session session) {
        if (userId.equals(session.getUserId()))
            return merge(session);
        return session;
    }

    @Override
    @SneakyThrows
    public void remove(@NotNull Session session) {
        String query = "DELETE FROM app_session WHERE  id = ? ";
        PreparedStatement preparedStatement = getJavaToMysql().prepareStatement(query);
        preparedStatement.setString(1, session.getId());
        preparedStatement.execute();
        preparedStatement.close();
    }

    @Override
    @SneakyThrows
    public void remove(@NotNull String userId, @NotNull Session session) {
        if (!userId.equals(session.getUserId()))
            return;

        remove(session);
    }

    @Override
    @SneakyThrows
    public void removeAll() {
        String query = "DELETE * FROM app_session";
        PreparedStatement preparedStatement = getJavaToMysql().prepareStatement(query);
        preparedStatement.execute();
        preparedStatement.close();
    }

    @Override
    @SneakyThrows
    public void removeAll(@NotNull Collection<Session> list) {
        for (Session session : list) {
            remove(session);
        }
    }

    @Override
    @SneakyThrows
    public void removeAll(@NotNull String userId) {
        for (Session session : findAll(userId)) {
            if (userId.equals(session.getUserId()))
                remove(session);
        }
    }

    @Override
    @SneakyThrows
    public void removeAll(@NotNull String userId, @NotNull Collection<Session> list) {
        for (Session session : list) {
            if (session.getUserId().equals(userId)){
                remove(session);
            }
        }
    }

    @Override
    @SneakyThrows
    public @NotNull Collection<Session> getCollection() {
        return getListSessions();
    }

    @Override
    @SneakyThrows
    public @NotNull Collection<Session> getCollection(@NotNull String userId) {
        Collection<Session> collections = new ArrayList<>();
        @NotNull final String query = "SELECT * FROM app_session WHERE id = ?";
        @NotNull PreparedStatement preparedStatement = getJavaToMysql().prepareStatement(query);
        preparedStatement.setString(1, userId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();

        while (resultSet.next()) collections.add(fetch(resultSet));
        preparedStatement.close();

        return collections;
    }

    @Nullable
    @SneakyThrows
    private Session fetch(@Nullable final ResultSet row) {
        if (row == null) return null;
        @NotNull final Session session = new Session();
        session.setId(row.getString("id"));
        session.setUserId(row.getString("user_id"));
        session.setSignature(row.getString("signature"));
        session.setDateStamp(new Date(row.getLong("timestamp")));

        return session;
    }

    @NotNull
    @SneakyThrows
    public List<Session> getListSessions() {
        @NotNull final List<Session> result = new ArrayList<>();
        @NotNull final String query = "SELECT * FROM app_session";
        @NotNull final ResultSet resultSet = getJavaToMysql().executeQuery(query);
        while (resultSet.next()) result.add(fetch(resultSet));
        resultSet.close();
        return result;
    }
}

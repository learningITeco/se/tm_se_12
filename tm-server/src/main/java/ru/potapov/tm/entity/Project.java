package ru.potapov.tm.entity;

import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.api.IEntity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@Getter
@Setter
@NoArgsConstructor
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public final class Project extends Entity {
    @Nullable private String      name;
    @Nullable private String      description;
    @Nullable private String      userId;
    @Nullable private Status      status;

    //@XmlJavaTypeAdapter(value = LacalDateAdapter.class)
//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd@HH:mm:ss.SSSZ")
    @Nullable private Date dateStart;

    //@XmlJavaTypeAdapter(value = LacalDateAdapter.class)
//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd@HH:mm:ss.SSSZ")
    @Nullable private Date dateFinish;

    public Project(@NotNull String name) {
        this();
        this.name       = name;
    }

    @Override
    public String toString() {
        return "Project [" + getName() + "]";
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;

        if (obj == null)
            return false;

        if (this.getClass() != obj.getClass())
            return false;

        Project objProject = (Project)obj;
        if ( (this.getName().equals(objProject.getName()))
                && (this.getDescription().equals(objProject.getDescription()))
                && (this.getDateStart().equals(objProject.getDateStart()))
                && (this.getDateFinish().equals(objProject.getDateFinish())) )
            return true;
        else
            return false;
    }

    @Override
    public int hashCode() {
        return getName().hashCode() + getDescription().hashCode()+ getDateStart().hashCode()+ getDateFinish().hashCode();
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}

package ru.potapov.tm.entity;

public enum FieldConst {

    ID("id"),
    NAME("name"),
    DESCRIPTION("description"),
    LOGIN("login"),
    HASHPASS("hashPass"),
    ROLETYPE("roleType")
    ;
    private String field;

    FieldConst(String field) {
        this.field = field;
    }
}

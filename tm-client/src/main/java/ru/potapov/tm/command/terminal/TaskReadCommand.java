package ru.potapov.tm.command.terminal;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.endpoint.Project;
import ru.potapov.tm.endpoint.Task;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
public final class TaskReadCommand extends TaskReadCommandAbstract {
    public TaskReadCommand(@NotNull final Bootstrap bootstrap) {
        super(bootstrap);
        setNeedAuthorize(true);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-read";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Reads tasks for a current project. Sorts by create[-c], by date start[-ds], by date finish[-df], by date status[-s] ";
    }

    @Override
    public void execute(@Nullable final String ... additionalCommands) throws Exception {
        super.execute();
        if (Objects.isNull(getServiceLocator()) || Objects.isNull(getBootstrap())
                || Objects.isNull(getBootstrap().getUserService().getAuthorizedUser())
                || Objects.isNull(getBootstrap().getUserService().getAuthorizedUser().getId())
                || Objects.isNull(getServiceLocator().getUserService().getAuthorizedUser()) )
            return;

        if (getBootstrap().getTaskService().checkSize() == 0){
            getServiceLocator().getTerminalService().printlnArbitraryMassage("We have not any project");
            return;
        }

        @Nullable Project findProject = null;
        boolean circleForProject = true;
        while (circleForProject) {
            getServiceLocator().getTerminalService().printlnArbitraryMassage("Input a project name for reading its tasks:");
            @NotNull String name = getServiceLocator().getTerminalService().getIn().nextLine();

            if ("exit".equals(name)){
                return;
            }

            findProject = getBootstrap().getProjectService().findProjectByName(name);

            if (Objects.isNull(findProject)) {
                getServiceLocator().getTerminalService().printlnArbitraryMassage("Project with name [" + name + "] does not exist, plz try again or type command <exit>");
                continue;
            }

            if (!getBootstrap().getUserService().getAuthorizedUser().getId().equals(findProject.getUserId())){
                getServiceLocator().getTerminalService().printlnArbitraryMassage("Project with name [" + name + "] does not belong to you!, plz try again or type command <exit>");
                continue;
            }

            circleForProject = false;
        }

        @NotNull final List<Task> list = (List<Task>)getBootstrap().getTaskService().findAll(getServiceLocator().getUserService().getAuthorizedUser().getId(), findProject.getId());
        Collections.sort(list, getComparator(sortBy));
        printTasksOfProject(findProject, list);
        getServiceLocator().getTerminalService().printMassageCompleted();
    }
}

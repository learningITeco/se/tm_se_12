package ru.potapov.tm.command.data.fasterxml.json;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.command.AbstractCommand;
import ru.potapov.tm.endpoint.User;

import java.util.Objects;

@NoArgsConstructor
public class DtoSaveFasterJson extends AbstractCommand {
    public DtoSaveFasterJson(@Nullable Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public @NotNull String getName() {
        return "save-faster-json";
    }

    @Override
    public @NotNull String getDescription() {
        return "Saves in json-format by FasterXML";
    }

    @Override
    public void execute(@Nullable String... additionalCommands) throws Exception {
        @Nullable final User user = getServiceLocator().getUserService().getAuthorizedUser();
        if (Objects.isNull(user) || !getServiceLocator().getUserService().isAdministrator( user ))
            return;
        if (!super.allowedRun())
            return;

        getServiceLocator().getProjectService().saveFasterJson(getServiceLocator().getSessionService().getSession());
        getServiceLocator().getTaskService().saveFasterJson(getServiceLocator().getSessionService().getSession());
        getServiceLocator().getTerminalService().printMassageCompleted();
    }
}

package ru.potapov.tm.command.data.jaxb.xml;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.command.AbstractCommand;
import ru.potapov.tm.endpoint.User;

import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
public class DtoLoadJaxbXml extends AbstractCommand {
    public DtoLoadJaxbXml(@Nullable Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public @NotNull String getName() {
        return "load-jaxb-xml";
    }

    @Override
    public @NotNull String getDescription() {
        return "Loads in xml-format by JAXB";
    }

    @Override
    public void execute(@Nullable String... additionalCommands) throws Exception {
        @Nullable final User user = getServiceLocator().getUserService().getAuthorizedUser();
        if (Objects.isNull(user) || !getServiceLocator().getUserService().isAdministrator( user ))
            return;
        if (!super.allowedRun())
            return;
        if (Objects.isNull(getServiceLocator()))
            return;

        getServiceLocator().getProjectService().loadJaxb(getServiceLocator().getSessionService().getSession(), true);
        getServiceLocator().getTaskService().loadJaxb(getServiceLocator().getSessionService().getSession(), true);
        getServiceLocator().getTerminalService().printMassageCompleted();
    }
}

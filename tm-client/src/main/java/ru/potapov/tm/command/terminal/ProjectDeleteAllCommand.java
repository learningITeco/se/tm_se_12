package ru.potapov.tm.command.terminal;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.command.AbstractCommand;

import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectDeleteAllCommand extends AbstractCommand {
    public ProjectDeleteAllCommand(@NotNull final Bootstrap bootstrap) {
        super(bootstrap);
        setNeedAuthorize(true);
    }

    @NotNull
    @Override
    public String getName() {
        return "projects-delete-all";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Deletes all project";
    }

    @Override
    public void execute(@Nullable final String ... additionalCommands) throws Exception {
        if (!super.allowedRun())
            return;
        if (Objects.isNull(getServiceLocator()) || Objects.isNull( getServiceLocator().getUserService().getAuthorizedUser()))
            return;

        getServiceLocator().getTaskService().removeAll( getServiceLocator().getUserService().getAuthorizedUser().getId() );
        getServiceLocator().getProjectService().removeAll( getServiceLocator().getUserService().getAuthorizedUser().getId() );
        getServiceLocator().getTerminalService().printMassageCompleted();
    }
}

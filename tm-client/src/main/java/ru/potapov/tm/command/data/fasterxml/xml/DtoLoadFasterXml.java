package ru.potapov.tm.command.data.fasterxml.xml;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.command.AbstractCommand;
import ru.potapov.tm.endpoint.User;

import java.util.Objects;

@NoArgsConstructor
public class DtoLoadFasterXml extends AbstractCommand {
    public DtoLoadFasterXml(@Nullable Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public @NotNull String getName() {
        return "load-faster-xml";

    }

    @Override
    public @NotNull String getDescription() {
        return "Loads from xml-format by FasterXML";
    }

    @Override
    public void execute(@Nullable String... additionalCommands) throws Exception {
        @Nullable final User user = getServiceLocator().getUserService().getAuthorizedUser();
        if (Objects.isNull(user) || !getServiceLocator().getUserService().isAdministrator( user ))
            return;
        if (!super.allowedRun())
            return;
        if (Objects.isNull(getServiceLocator()))
            return;

        getServiceLocator().getProjectService().loadFasterXml(getServiceLocator().getSessionService().getSession());
        getServiceLocator().getTaskService().loadFasterXml(getServiceLocator().getSessionService().getSession());
        getServiceLocator().getTerminalService().printMassageCompleted();
    }
}

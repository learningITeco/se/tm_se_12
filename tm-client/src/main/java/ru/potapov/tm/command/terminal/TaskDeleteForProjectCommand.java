package ru.potapov.tm.command.terminal;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.command.AbstractCommand;
import ru.potapov.tm.endpoint.Project;
import ru.potapov.tm.endpoint.Task;

import java.util.Collection;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
public final class TaskDeleteForProjectCommand extends AbstractCommand {
    public TaskDeleteForProjectCommand(@NotNull final Bootstrap bootstrap) {
        super(bootstrap);
        setNeedAuthorize(true);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-delete-p";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Deletes all tasks of a project";
    }

    @Override
    public void execute(@Nullable final String ... additionalCommands) throws Exception {
        if (!super.allowedRun())
            return;
        if (Objects.isNull(getServiceLocator()))
            return;

        if (getServiceLocator().getTaskService().checkSize() == 0){
            getServiceLocator().getTerminalService().printlnArbitraryMassage("We do not have any task.");
            return;
        }

        @Nullable Project findProject = null;
        boolean circleForProject = true;
        while (circleForProject) {
            @NotNull String name = getServiceLocator().getTerminalService().readLine("Input a project name for removing its tasks:");

            if ("exit".equals(name)){
                return;
            }

            findProject = getServiceLocator().getProjectService().findProjectByName(getServiceLocator().getUserService().getAuthorizedUser().getId(), name);

            if (Objects.isNull(findProject)) {
                getServiceLocator().getTerminalService().printlnArbitraryMassage("Project with name [" + name + "] does not exist, plz try again or type command <exit>");
                continue;
            }

            if (!getServiceLocator().getUserService().getAuthorizedUser().getId().equals(findProject.getUserId()) && getServiceLocator().getUserService().isAdministrator( getServiceLocator().getUserService().getAuthorizedUser() )){
                getServiceLocator().getTerminalService().printlnArbitraryMassage("Project with name [" + name + "] does not belong to you!, plz try again or type command <exit>");
                continue;
            }

            circleForProject = false;
        }

        @NotNull Collection<Task> listTaskRemoving = getServiceLocator().getTaskService().findAll(findProject.getId());

        getServiceLocator().getTaskService().removeAll(listTaskRemoving);
        getServiceLocator().getTerminalService().printMassageCompleted();
    }
}

package ru.potapov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.endpoint.*;

import java.util.Collection;

public interface ITaskService extends IService {
    int checkSize() throws ValidateExeption_Exception;
    @Nullable Task findTaskByName(@NotNull final String name) throws ValidateExeption_Exception;
    void remove(@NotNull final Task task) throws ValidateExeption_Exception;
    void removeAll(@NotNull final String userId) throws ValidateExeption_Exception;
    void removeAll(@NotNull final Collection<Task> listTasks) throws ValidateExeption_Exception;
    @NotNull Task renameTask(@NotNull final Task task, @NotNull final String name) throws CloneNotSupportedException_Exception, ValidateExeption_Exception;
    void changeProject(@NotNull final Task task, @NotNull final Project project) throws CloneNotSupportedException_Exception, ValidateExeption_Exception;
    @NotNull Collection<Task> findAll(@NotNull final String projectId) throws ValidateExeption_Exception;
    @NotNull Collection<Task> findAll(@NotNull final String userId, @NotNull final String projectId) throws ValidateExeption_Exception;
    @NotNull Collection<Task> findAllByUser(@NotNull final String userId) throws ValidateExeption_Exception;
    void put(Task task) throws ValidateExeption_Exception;
    @NotNull String collectTaskInfo(@NotNull final Task task) throws ValidateExeption_Exception;
//    void setMapRepository(@NotNull final Map<String, Task> mapRepository) throws ValidateExeption_Exception;
//    @NotNull Map<String, Task> getMapRepository() throws ValidateExeption_Exception;
}

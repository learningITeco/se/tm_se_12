package ru.potapov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.endpoint.Session;

public interface ISessionService {
    @Nullable boolean checkUserSession();
    void setSession(@NotNull Session session);
    void removeSession(@NotNull Session session);
    @Nullable Session getSession();
}

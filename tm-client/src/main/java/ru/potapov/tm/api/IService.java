package ru.potapov.tm.api;

import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.endpoint.Session;

public interface IService {
    void loadBinar(@Nullable Session session) throws Exception;
    void saveBinar(@Nullable Session session) throws Exception;

    void saveJaxb(@Nullable Session session, final boolean formatXml) throws Exception;
    void loadJaxb(@Nullable Session session, final boolean formatXml) throws Exception;

    void saveFasterXml(@Nullable Session session) throws Exception;
    void loadFasterXml(@Nullable Session session) throws Exception;

    void saveFasterJson(@Nullable Session session) throws Exception;
    void loadFasterJson(@Nullable Session session) throws Exception;
}
